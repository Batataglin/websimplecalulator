
function Add()
{
    let num1 = parseInt(document.getElementById("num1").value);
    let num2 = parseInt(document.getElementById("num2").value);

    document.getElementById("answer").innerHTML = num1 + num2;
}

function Subtract()
{
    let num1 = parseInt(document.getElementById("num1").value);
    let num2 = parseInt(document.getElementById("num2").value);

    document.getElementById("answer").innerHTML = num1 - num2;
}

function Multiply()
{
    let num1 = parseInt(document.getElementById("num1").value);
    let num2 = parseInt(document.getElementById("num2").value);

    document.getElementById("answer").innerHTML = num1 * num2;
}

function Divide()
{
    let num1 = parseInt(document.getElementById("num1").value);
    let num2 = parseInt(document.getElementById("num2").value);

    document.getElementById("answer").innerHTML = num1 / num2;
}

function Square(num)
{
    return (num * num);
}
